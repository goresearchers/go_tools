# README #

You need cmake at least at version 2.6. Go get it.

apt-get install cmake

You need to install SFML 2.1 to get the code to work.
In Linux, you can do this by installing from the current repo.
Like: (Debian Based)

apt-get install libcsfml-dev libsfml

You also need to install boost, at least at version 1.55

apt-get install libboost1.55-all-dev 

Also, do not forget to copy the Images directory to where your executable is.
For example, if through cmake you create another Build directory, there will be a folder called GoGui. There it is where your executable is, copy the Images folder to there and you are good to go.

gabrielmsantos@gmail.com